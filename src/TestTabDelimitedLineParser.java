import ninja.aggro.*;

import java.io.IOException;
import java.util.List;
import java.lang.IllegalArgumentException;

public class TestTabDelimitedLineParser {

    public static void main(String[] args) throws IOException, IllegalArgumentException {
        if (args.length == 0) {
            printUsage();
            return;
        }

        ILineParser lineParser = LineParserFactory.getLineParser(args[0]);

      /* display results */
        System.out.println("\nResults:");
        List<String> listTokens = null;
        int intLine = 0;
        while ((listTokens = lineParser.getNexLineTokens()) != null) {
            System.out.print("  " + (++intLine) + ": ");
            listTokens.forEach(strItem -> System.out.print("[" + strItem + "] "));
            System.out.println();
        }
        System.out.println();
    }

    public static void printUsage() {
        System.err.println("Missing argument: filename");
        System.out.println("Usage:");
        System.out.println("  TestTabDelimitedLineParser <filename>\n");
    }
}
