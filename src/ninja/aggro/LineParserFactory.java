package ninja.aggro;

import java.util.List;
import java.io.*;
import java.nio.file.*;
import java.util.stream.*;
import java.lang.IndexOutOfBoundsException;

public class LineParserFactory {
    /**
     * Creates and returns the proper ILineParser required for the specified
     * file.
     *
     * @param filename path to the file to be parsed
     * @return an instance of ILineParser specific to your needs
     */
    public static ILineParser getLineParser(String strFilename) throws IOException, IllegalArgumentException {
        /* In the interest of time and not using any external dependencies,
         * I give you ghetto file extension detection. This, of course,
         * won't deal well when it comes to files with multiple extensions. */
        String strFileExtension = null;
        try {
            strFileExtension = strFilename.substring(strFilename.lastIndexOf('.'));
        } catch (IndexOutOfBoundsException e) {
            throw new IllegalArgumentException(strFilename + ": Filename does not have an extension");
        }

        Stream<String> streamLines = Files.lines(Paths.get(strFilename));

       /* We could get fancy and do something like register each LineParser class
        * dynamically with the factory. In this case, for such a small amount of
        * possible file extension options, we'll just go with a classic switch */
        switch (strFileExtension) {
            case ".tab":
                return new TabDelimitedLineParser(streamLines);
            /* Room to grow... */
            //case ".csv":
            //  return new CommaDelimitedLineParser(streamLines);
            // .
            // .
            // .
        }

        streamLines.close();
        throw new IllegalArgumentException(strFilename + ": File does not have valid file extension");
    }
}
