package ninja.aggro;

import java.util.List;
import java.io.*;

/**
 * Interface for the line parser classes
 */
public interface ILineParser {

    /**
     * Iterates through given file, parsing it, and returning it's tokens line by line.
     *
     * @return List of a line's items split by their seperator.
     */
    public List<String> getNexLineTokens() throws IOException;

}
