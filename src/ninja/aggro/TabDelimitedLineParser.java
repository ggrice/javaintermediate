package ninja.aggro;

import java.util.List;
import java.io.*;
import java.util.*;
import java.util.stream.*;

/**
 * Fabled line parser able to tokenize string items separated by '\t'.
 */
public class TabDelimitedLineParser implements ILineParser {

    private Stream<String> streamLineIn;
    private Iterator<String> iterLine;

    /**
     * @param streamLineIn An opened file stream for the file to be parsed
     */
    public TabDelimitedLineParser(Stream<String> streamLineIn) {
        this.streamLineIn = streamLineIn;
        this.iterLine = this.streamLineIn.iterator();
    }

    public List<String> getNexLineTokens() throws IOException {
        try {
            return Arrays.asList(iterLine.next().trim().split("\t"));
        } catch (NoSuchElementException e) {
            if (streamLineIn != null) {
                streamLineIn.close(); // Done with the file at this point,
                                      // there is nothing like a rewind() so might as well make sure
                                      // the file gets closed here since we're not using this in
                                      // try-with-resources
                streamLineIn = null;
            }
            return null;
        }
    }
}
